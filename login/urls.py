from django.conf.urls import url

from . import views

urlpatterns = [
        url(r'^logout', views.index, name='logout'),
        url(r'^signup', views.signUp, name='signup'),
        url(r'^$', views.index, name='login'),
]
