from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.template import loader
from django.contrib.auth.models import User
from django.contrib.auth import authenticate
import pdb;


def index(request):
    if request.method == "POST":
        authenticate(username=request.POST['username'], password=request.POST['password'])
    else :
        template = loader.get_template('login/index.html')
        return HttpResponse(template.render({}, request))

def logout(request):
    #XXX : Handle auth
    template = loader.get_template('login/index.html')
    return HttpResponse(template.render({}, request))

def signUp(request):
    #XXX : Handle auth
    if request.method == "POST":
        # pdb.set_trace();
        user = User.objects.create_user(request.POST['username'], request.POST['email'], request.POST['password']);
        user.first_name = request.POST['firstname'];
        user.last_name = request.POST['surname'];
        user.save();
        return redirect('/');
    else:
        template = loader.get_template('login/signup.html')
        return HttpResponse(template.render({}, request))
